# MAPA 1 - La consultoría.
```plantuml
@startmindmap
*[#B5EAEA]	La consultoría
**[#EDF6E5]	Es un servicio que se les brinda a\nlas empresas para que estas puedan\nseguir ofreciendo sus servicios a la par\nde seguir innovando.
***[#FFBCBC]	Servicios y conocimiento.
****[#F38BA0] Una empresa de consultoría\nrequiere saber de:
*****[#F39189] Conocimiento sectorial.
*****[#F39189] Conocimiento técnico.
*****[#F39189] Capacidad de plantear soluciones.
****[#F38BA0] Tipología de servicios.
*****[#F39189] Consultoría.
******[#BB8082] Reingeniería de procesos.
******[#BB8082] Gobierno TI.
******[#BB8082] Oficina de proyectos.
******[#BB8082] Analítica avanzada de datos.
*****[#F39189] Integración.
******[#BB8082] Desarrollos a medida.
******[#BB8082] Calidad E2E.
******[#BB8082] Infraestructuras.
******[#BB8082] Soluciones de mercado.
*****[#F39189] Externalización.
******[#BB8082] Gestión de aplicaciones.
******[#BB8082] Servicios SQA.
******[#BB8082] Operación y adm. de infra.
******[#BB8082] Procesos de negocio.
*****[#F39189] Servicios de estrategia.
******[#BB8082] Ayudan a las compañías a definir\nla organización interna que deben\ntener los sistemas, planes y modelos\n de negocio.
***[#FFBCBC]	Profesión de futuro.
****[#F38BA0] Es un modo de vida, que dependiendo\nde los estándares individuales, te\npermitirá alcanzar la felicidad profesional.
*****[#F39189] Retribución competitiva.
*****[#F39189] Carrera profesional.
*****[#F39189] Retos.
****[#F38BA0] Nivel importante de crecimiento y\nde carrera profesional.
****[#F38BA0] El sector económico crece año con año.
***[#FFBCBC]	¿Qué exige la consultoría?
****[#F38BA0] Buenos profesionales.
*****[#F39189] Por su:
******[#BB8082] Formación y experiencia.
******[#BB8082] Capacidad de trabajo y evolución.
*****[#F39189] Capaces de:
******[#BB8082] Colaborar en un proyecto común.
******[#BB8082] Trabajar en equipo.
******[#BB8082] Ofrecer honestidad y sinceridad.
****[#F38BA0] Proactividad.
****[#F38BA0] Voluntad de mejora.
****[#F38BA0] Responsabilidad.
****[#F38BA0] Disponibilidad total.
***[#FFBCBC] ¿Cómo es la carrera profesional\nen la consultoría?
****[#F38BA0] Generalmente se gestiona por categorías y\nse va avanzando según la experiencia\nque se vaya adquiriendo.
*****[#F39189] Categorías en AXPE:
******[#BB8082] Primera etapa: Junior.
******[#BB8082] Segunda etapa: Senior.
******[#BB8082] Tercera etapa: Gerente.
******[#BB8082] Cuarta etapa: Director.
*****[#F39189] Plan de formación.
@endmindmap
```

# MAPA 2 - Consultoría de software.
```plantuml
@startmindmap
*[#B5EAEA]	Consultoría de Software.
**[#EDF6E5] 	Actividades de una consultora.
***[#FFBCBC] Desarrollo de software.
****[#F38BA0] Nace de una necesidad que el\ncliente tenga.
****[#F38BA0] Determinar los requerimentos\nnecesarios.
****[#F38BA0] Estudio de viabilidad.
*****[#F39189] El costo que tendrá.
*****[#F39189] Las implicaciones que conlleva.
*****[#F39189] Ahorros que se pueden hacer.
*****[#F39189] Decidir si es viable o no.
****[#F38BA0] Diseño funcional.
*****[#F39189] Determinar la información\nnecesaria por el sistema.
******[#BB8082] Entrada.
******[#BB8082] Salida.
*****[#F39189] Modelo de datos.
*****[#F39189] Prototipo.
****[#F38BA0] Diseño técnico.
*****[#F39189] Trasladar los requerimentos a un\nlenguaje técnico y establecer las\nherramientas necesarias para su\ndesarrollo.
*****[#F39189] Realizar pruebas.
****[#F38BA0] Instalación del software a los\ndispositivos del cliente.
***[#FFBCBC] Factoría de software.
****[#F38BA0] Su función es "industrializar"\nla creación del software, abaratando\nsus costos y facilitando el desarrollo\nde más proyectos.
***[#FFBCBC] Crear y mantener software para\nempresas.
****[#F38BA0] Crear infraestructura.
*****[#F39189] Computadoras.
*****[#F39189] Servidores.
*****[#F39189] Conexiones de comunicación.
*****[#F39189] Aplicaciones.
****[#F38BA0] Hacer funcionales las comunicaciones.
****[#F38BA0] Aclopamiento a proyectos grandes\n"Factor escala".
**[#EDF6E5] ¿Cómo es el trabajo en una consultora?
***[#FFBCBC] Cada vez está más dividido por\nsectores especializados en un área\nen general, o un área específica\ncomo pueden ser el especializarse en\nun cliente en concreto.
**[#EDF6E5] ¿Cuáles son las tendencias?
***[#FFBCBC] Se ha renovado el concepto de\ninfraestructura de una empresa.
****[#F38BA0] Anteriormente las empresas tenían\nsus propios ordenadores y todo lo\nnecesario para que funcionacen\ncorrectamente.
****[#F38BA0] Ahora toda esa información está\nalmacenada en grandes servidores\nde diferentes empresas, como\ngoogle o amazon.
***[#FFBCBC] Puede gestionarse de mucho mejor\nmanera la información que la\nempresa requiere.
***[#FFBCBC] Ya no se utilizan bases de datos\ntradicionales, o relacionales\ncon el fin de procesar la\ninformación mucho más rápido.
***[#FFBCBC] Cada vez se automatizan más los\ntrabajos.
@endmindmap
```

# MAPA3 - Aplicación de la ingeniería de software
```plantuml
@startmindmap
*[#B5EAEA] Aplicación de la\ningeniería de software
**[#EDF6E5] Aplicación práctica.
***[#FFBCBC] SAP
****[#F38BA0] Empresa especializada en la\ngestión empresarial, la primera en\nsu tipo en abarcar tantos niveles.
****[#F38BA0] Enfoque metodológico.
*****[#F39189] Requisitos.
*****[#F39189] Áreas de aplicación.
*****[#F39189] Reunión con los responsables.
*****[#F39189] Visión global.
*****[#F39189] Selección de procesos de la empresa.
*****[#F39189] Documentación de requisitos.
*****[#F39189] Análisis funcional.
*****[#F39189] Análisis técnico.
******[#BB8082] Programación.
*******[#6E7582] Consultores funcionales.
********[#046582] Uso de las herramientas.
********[#046582] Desarrollo de los programas.
*********[#34626C] Documentación.
**********[#839B97] Diseño técnico
**********[#839B97] Definir variables.
**********[#839B97] Autónomo a las pruebas iniciales.
**********[#839B97] Integración con los procesos.
******[#BB8082] Parametrización.
*****[#F39189] Prototipado.
******[#BB8082] Gestión de alcance o\ncambios de alcance.
*****[#F39189] Rentabilidad de proyecto.
*****[#F39189] Fase de pruebas.
*****[#F39189] Entrega del proyecto.
****[#F38BA0] Codificación de todos los documentos.
***[#FFBCBC] Soluciones que cubran el estándar\ndel negocio, de acuerdo a sus\nnecesidades particulares.
****[#F38BA0] Partes especializadas para cada\nsector, si la empresa es grande\no más generales si la empresa\nes pequeña.
****[#F38BA0] Herramientas específicas para\nlas diferentes etapas.
****[#F38BA0] Diseño funcional.
*****[#F39189] Requisitos técnicos.
*****[#F39189] Trazabilidad.
*****[#F39189] Soporte de cambios.
@endmindmap
```
